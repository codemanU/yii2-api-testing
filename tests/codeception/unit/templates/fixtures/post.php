<?php

/**
 * Created by PhpStorm.
 * User: wp-24
 * Date: 24.05.2016
 * Time: 18:20
 */

return [
    'id' => $faker->unique()->numberBetween($min = 3, $max = 100),
    'created_at' => $faker->unixTime,
    'updated_at' => $faker->unixTime,
    'title' => $faker->sentence(5, true),
    'userId' => 2,
    'description' => $faker->sentence(50, true),  // generate a sentence with 7 words
];