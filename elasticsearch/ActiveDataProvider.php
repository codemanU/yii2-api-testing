<?php


namespace app\elasticsearch;

use yii\data\ActiveDataProvider as BaseDataProvider;
use yii\helpers\ArrayHelper;

class ActiveDataProvider extends BaseDataProvider
{
    public $sourceAttribute = 'source';

    public $key = 'id';

    public function getModels($source = true)
    {
        return ArrayHelper::getColumn(parent::getModels(), $this->sourceAttribute);
    }
}