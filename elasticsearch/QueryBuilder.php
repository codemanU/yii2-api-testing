<?php


namespace app\elasticsearch;


use yii\elasticsearch\QueryBuilder as BaseQueryBuilder;
use yii\helpers\Json;


class QueryBuilder extends  BaseQueryBuilder
{
    public function build($query)
    {
        $parts = [];

        if ($query->fields === []) {
            $parts['fields'] = [];
        } elseif ($query->fields !== null) {
            $fields = [];
            $scriptFields = [];
            foreach($query->fields as $key => $field) {
                if (is_int($key)) {
                    $fields[] = $field;
                } else {
                    $scriptFields[$key] = $field;
                }
            }
            if (!empty($fields)) {
                $parts['fields'] = $fields;
            }
            if (!empty($scriptFields)) {
                $parts['script_fields'] = $scriptFields;
            }
        }
        if ($query->source !== null) {
            $parts['_source'] = $query->source;
        }
        if ($query->limit !== null && $query->limit >= 0) {
            $parts['size'] = $query->limit;
        }
        if ($query->offset > 0) {
            $parts['from'] = (int) $query->offset;
        }

        if (empty($query->query)) {
            $parts['query'] = ["match_all" => (object) []];
        } else {
            $parts['query'] = $query->query;
        }

        $whereFilter = $this->buildCondition($query->where);
        if (is_string($query->filter)) {
            if (empty($whereFilter)) {
                $parts['filter'] = $query->filter;
            } else {
                $parts['filter'] = '{"and": [' . $query->filter . ', ' . Json::encode($whereFilter) . ']}';
            }
        } elseif ($query->filter !== null) {
            if (empty($whereFilter)) {
                $parts['filter'] = $query->filter;
            } else {
                $parts['filter'] = ['and' => [$query->filter, $whereFilter]];
            }
        } elseif (!empty($whereFilter)) {
            $parts['filter'] = $whereFilter;
        }

        // patch
        if (isset($parts['filter'])) {
            $parts['query'] = [
                'filtered' => [
                    'query' => $parts['query'],
                    'filter' => $parts['filter'],
                ]
            ];
            unset($parts['filter']);
        }

        if (!empty($query->highlight)) {
            $parts['highlight'] = $query->highlight;
        }
        if (!empty($query->aggregations)) {
            $parts['aggregations'] = $query->aggregations;
        }
        if (!empty($query->stats)) {
            $parts['stats'] = $query->stats;
        }
        if (!empty($query->suggest)) {
            $parts['suggest'] = $query->suggest;
        }

        $sort = $this->buildOrderBy($query->orderBy);
        if (!empty($sort)) {
            $parts['sort'] = $sort;
        }

        $options = [];
        if ($query->timeout !== null) {
            $options['timeout'] = $query->timeout;
        }

        return [
            'queryParts' => $parts,
            'index' => $query->index,
            'type' => $query->type,
            'options' => $options,
        ];
    }

}