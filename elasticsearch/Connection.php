<?php


namespace app\elasticsearch;

use yii\elasticsearch\Connection as BaseConnection;

class Connection extends BaseConnection
{
    public function getQueryBuilder()
    {
        return new QueryBuilder($this);
    }
}