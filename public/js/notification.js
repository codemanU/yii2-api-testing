/**
 * Created by wp-24 on 14.06.2016.
 */
$( document ).ready(function() {

    var socket = io.connect('http://192.168.11.118:8890');

    socket.on('notification', function (data) {

        var message = JSON.parse(data);

        $( "#notifications" ).prepend( "<p><strong>" + message.name + "</strong>: " + message.message + "</p>" );

    });

});