<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii;
use app\modules\posts\Post;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
//        $role = Yii::$app->authManager->createRole('admin');
//        $role->description = 'Admin';
//        Yii::$app->authManager->add($role);
//
//        $role = Yii::$app->authManager->createRole('user');
//        $role->description = 'User';
//        Yii::$app->authManager->add($role);
    }

    public function actionTest()
    {
        Yii::$app->set('request', new \yii\web\Request());
        $user = new User(['id' => 1, 'username' => 'test']);
        Yii::$app->user->login($user);
//        var_dump(Yii::$app->user->id);
    }

    public function actionFakePosts()
    {
        for ($i=0; $i < 20; $i++) {

            $faker = \Faker\Factory::create();
            $post = new Post();

            $post->title = $faker->sentence(5, true);
            $post->userId = 2;
            $post->description = $faker->sentence(20, true);
            if (!$post->save()) {
                print_r($post->getErrors());
            }
        }
    }
}
