<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "access_token".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $userId
 * @property integer $expires_in
 * @property string $token
 */
class AccessToken extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'access_token';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'expires_in', 'token'], 'required'],
            [['userId', 'expires_in'], 'integer'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'userId' => 'User ID',
            'expires_in' => 'Expires In',
            'token' => 'Token',
        ];
    }

    public function getTokenByUser(User $user)
    {
        $token = self::findOne(['userId' => $user->id]);
        if (!$token) {
            $token = $this->setNewToken($user->id);
        }
        return $token->token;
    }

    public function setNewToken ($id)
    {
        //30 days token expires
        $this->token = Yii::$app->security->generateRandomString(100);
        $this->expires_in = time() + 30*24*60*60;
        $this->userId = $id;
        if (!$this->save()) {
            print_r($this->getErrors());die;
        }
        return $this->token;
    }

}
