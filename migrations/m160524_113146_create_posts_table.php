<?php

use yii\db\Migration;

/**
 * Handles the creation for table `posts_table`.
 */
class m160524_113146_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
 
        $this->createTable('{{%posts}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'userId' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'description' => $this->string(),
        ], $tableOptions);

        $this->createIndex('idx-posts-id', '{{%posts}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts_table');
    }
}
