<?php

use yii\db\Migration;

/**
 * Handles the creation for table `access_token_table`.
 */
class m160615_084856_create_access_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('access_token_table', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'userId' => $this->integer()->notNull(),
            'expires_in' => $this->integer()->notNull(),
            'token' => $this->string(255)->notNull()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('access_token_table');
    }
}
