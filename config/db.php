<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2testing',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
