<?php
/**
 * Created by PhpStorm.
 * User: wp-24
 * Date: 15.06.2016
 * Time: 11:11
 */
namespace app\modules\api\controllers;


class PostsController extends DefaultController
{

    public $modelClass = 'app\modules\posts\Post';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
}
