<?php

namespace app\modules\chat\controllers;

use yii\web\Controller;
use yii\helpers\Json;
use Yii;

/**
 * Default controller for the `chats` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        if (Yii::$app->request->post()) {

            $name = Yii::$app->request->post('name');
            $message = Yii::$app->request->post('message');

            Yii::$app->redis->executeCommand('PUBLISH', [
                'channel' => 'notification',
                'message' => Json::encode(['name' => $name, 'message' => $message])
            ]);
            return;

        }

        return $this->render('index');
    }
}
