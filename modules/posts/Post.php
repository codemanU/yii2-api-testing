<?php

namespace app\modules\posts;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;


/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $userId
 * @property string $title
 * @property string $description
 */
class Post extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['userId', 'required'],
            ['userId', 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'userId' => 'User ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    public function getUsername()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
