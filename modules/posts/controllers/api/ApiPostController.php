<?php
/**
 * Created by PhpStorm.
 * User: wp-24
 * Date: 14.06.2016
 * Time: 12:34
 */
namespace app\modules\posts\controllers\api;

use yii\rest\ActiveController;
use yii\web\Response;

class ApiPostController extends ActiveController
{
    public $modelClass = 'app\modules\posts\Post';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

}